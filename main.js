import React from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  ImageBackground
} from 'react-native';
import {connect} from 'react-redux';

const mapStateToProps=(state)=>{
  return{

  }
}

const mapDispatchToProps=(dispatch)=>{
  return{

  }
}

class ScrollComponent extends React.Component{
  constructor(props){
    super(props)
  }

  render(){
    let {
      scrollHorizontal,
      componentList,
      containerStyle,
      heading,
      subHeading,
      headingStyle,
      subHeadingStyle,
      scrollEnabled,
      rightTitleObject,
      rightTitleStyle,
      bottomTextObject,
      marginBetween
    } = this.props;

    let bottomSection=(
      bottomTextObject?(
        <View style={{justifyContent:'center',alignItems:'center'}}>
          <TouchableOpacity onPress={()=>{
            bottomTextObject.action()
          }}>
            <Text style={bottomTextObject.style}>{bottomTextObject.text}</Text>
          </TouchableOpacity>
        </View>
      ):(<View></View>)
    )

    return(
      <View style={containerStyle}>
          <Text style={headingStyle}>{heading}</Text>
          <View style={{flexDirection:'row',justifyContent:'space-between'}}>
            <Text style={subHeadingStyle}>{subHeading}</Text>
            <TouchableOpacity onPress={()=>{
              rightTitleObject.action()
            }} activeOpacity={rightTitleObject.action?2:0}>
              <Text style={rightTitleObject.style}>{rightTitleObject.title}</Text>
            </TouchableOpacity>
          </View>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          scrollEnabled={scrollEnabled?scrollEnabled:true}
          horizontal={scrollHorizontal}
          contentContainerStyle={[{marginTop:marginBetween?marginBetween:20}]}>
            {componentList}
            {bottomSection}
        </ScrollView>
      </View>
    )
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(ScrollComponent)
